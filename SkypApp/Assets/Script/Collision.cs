﻿using UnityEngine;
using System.Collections;

public class Collision : MonoBehaviour {

	public delegate void CollisionEnterAction();
	public static event CollisionEnterAction OnTriggerEnterAction;

	void OnTriggerEnter2D(Collider2D other) {
		if (OnTriggerEnterAction != null)
			OnTriggerEnterAction ();
		//Destroy(this.gameObject);
		print ("Collision !");
	}
}
