﻿using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour {

	/// <summary>
	/// The jump force.
	/// </summary>
	public float jumpForce;
	/// <summary>
	/// The cooldown time in seconds.
	/// </summary>
	public float cooldownTime = 1;
	float cooldownTimer;
	// Jump states
	enum state{idle = 0, jumping, cooldown};
	state jumpState;
	// Player's rigidbody
	Rigidbody2D playerRigidBody;
	
	void Start () {
		jumpState = state.idle;
		playerRigidBody = GetComponent<Rigidbody2D> ();
	}
	
	void Update () {
		
		switch (jumpState) {
			
		case state.jumping:
			playerRigidBody.AddForce(new Vector2(0,jumpForce),ForceMode2D.Impulse);
			jumpState = state.cooldown;
			break;
		case state.cooldown:
			cooldownTimer += Time.deltaTime;
			if(cooldownTimer >= cooldownTime) {
				jumpState = state.idle;
				cooldownTimer = 0;
			}
			break;
		default:
			break;
		}
	}
	
	void InstantTouch()
	{
		if(jumpState == state.idle)
			jumpState = state.jumping;
		print("You touched");
	}
	
	void OnEnable()
	{
		InputController.InstantTouch += InstantTouch;
	}
	
	void OnDisable()
	{
		InputController.InstantTouch -= InstantTouch;
	}
}
