﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

	public delegate void TouchAction();
	public static event TouchAction InstantTouch;

	// Update is called once per frame
	void Update () {
		foreach (Touch touch in Input.touches) {
			if(touch.phase == TouchPhase.Began)
			{
				if(InstantTouch != null)
				{
					InstantTouch();
					print("You touched");
				}
					

			}

		}
	}
}
