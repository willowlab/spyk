﻿using UnityEngine;
using System.Collections;

public class TranslationScript : MonoBehaviour {
	
	public Vector2 finalPoint;
	// Use this for initialization
	void Start () {
		finalPoint = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		moveX();
		moveY();
		
	}
	
	void moveX()
	{
		if (gameObject.transform.position.x > finalPoint.x) {
			gameObject.transform.Translate (-Vector2.right * Time.deltaTime);
		} 
		else if (gameObject.transform.position.x < finalPoint.x) {
			gameObject.transform.Translate (Vector2.right * Time.deltaTime);
		}
	}
	
	void moveY()
	{
		if (gameObject.transform.position.y > finalPoint.y) {
			gameObject.transform.Translate (-Vector2.up * Time.deltaTime);
		} 
		else if (gameObject.transform.position.y < finalPoint.y) {
			gameObject.transform.Translate (Vector2.up * Time.deltaTime);
		}
	}
}
